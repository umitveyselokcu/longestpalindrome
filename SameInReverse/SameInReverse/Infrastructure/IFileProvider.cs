namespace SameInReverse.Infrastructure
{
    public interface IFileProvider
    {
        void WriteToFile(string text, string fileName);
        string ReadFromFile(string fileName);
    }
}