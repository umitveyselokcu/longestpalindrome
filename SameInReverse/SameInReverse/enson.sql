
create table customers (
    entity_id                    number not null constraint entity_id_pk primary key,
    name                           varchar2(55),
    surname                        varchar2(55),
    date_of_birth                  date,
    place_of_birth                 varchar2(55)
);
/
create table clients (
    entity_id                    number constraint clients_entity_id_unq unique,
    name                           varchar2(55),
    surname                        varchar2(55),
    date_of_birth                  date,
    place_of_birth                 varchar2(100),
    row_id                    number
);
/


create sequence customers_seq
  START WITH 1
  INCREMENT BY 1
  CACHE 100;
/
create trigger customers_before_insert
    before insert or update 
    on customers
    for each row
begin
    if :new.entity_id is null then
        :new.entity_id := customers_seq.nextval;
    end if;
end customers_before_insert;
/

alter table customers add constraint place_of_birth_unq unique (place_of_birth);
/


Declare
    rows_inserted number := 0;
Begin
    loop
        Begin
            INSERT INTO customers( name, surname, date_of_birth, place_of_birth) VALUES( 
            dbms_random.string('L', 17),
            dbms_random.string('L', 15),
            sysdate ,
            dbms_random.string('L', 15)                
            );
            rows_inserted := rows_inserted + 1;
        Exception When DUP_VAL_ON_INDEX Then Null;
        End;
        exit when rows_inserted = 15;
    End loop;
COMMIT;
End;
/

alter table customers drop constraint place_of_birth_unq;
/






create or replace package sodexo_package
    as    
    function scheduled_function_customers_to_clients 
     return number;
end sodexo_package;
/
create or replace package body sodexo_package
    as
   
    FUNCTION scheduled_function_customers_to_clients 
        RETURN NUMBER
        IS    
            l_total_sales NUMBER := 0;
            PRAGMA AUTONOMOUS_TRANSACTION;
        BEGIN
          FOR r_product IN ( SELECT ROWNUM, customers.* FROM customers where entity_id NOT IN (SELECT entity_id FROM clients ) )
              LOOP
                IF (MOD(r_product.ROWNUM,2) = 0) THEN        
                    INSERT INTO clients (entity_id, name, surname, date_of_birth, place_of_birth,row_id) 
                    Values  (r_product.entity_id, r_product.name, r_product.surname, r_product.date_of_birth, r_product.place_of_birth,r_product.ROWNUM);
                    COMMIT;
                END IF;
                
              END LOOP;
          return l_total_sales;
    END scheduled_function_customers_to_clients;
end sodexo_package;
/


BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'copy_evens',
   job_type           =>  'PLSQL_BLOCK',
   job_action         =>  'BEGIN sodexo_package.scheduled_function_customers_to_clients(); END;' ,
   start_date         =>  '28-APR-08 07.00.00 PM Australia/Sydney',
   repeat_interval    =>  'FREQ=DAILY;INTERVAL=2',
   end_date           =>  '20-NOV-08 07.00.00 PM Australia/Sydney',
   auto_drop          =>   FALSE,
   job_class          =>  'DBMS_JOB',
   comments           =>  'My new job');
END;
/

