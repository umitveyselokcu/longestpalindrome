using System;
using SameInReverse.Infrastructure;

namespace SameInReverse
{
    public class ProblemSolver
    {
        private readonly ISearchService _searchService;
        private readonly IFileProvider _fileProvider;

        public ProblemSolver(ISearchService searchService,
            IFileProvider fileProvider)
        {
            _searchService = searchService;
            _fileProvider = fileProvider;
        }
        public void Solve()
        {
            var textInFile = _fileProvider.ReadFromFile("SameInReverse.sdx");
            var sameInReverseCharset = _searchService.GetLongestPalindrome(textInFile);
            _fileProvider.WriteToFile(sameInReverseCharset,"SameInReverseFound.sdx");
        }
    }
}