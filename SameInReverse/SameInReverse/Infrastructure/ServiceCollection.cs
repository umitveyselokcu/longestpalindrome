using System;
using System.Collections.Generic;
using System.Reflection;

namespace SameInReverse.Infrastructure  
{
    public static class ServiceCollection 
    {
        public static void RegisterServices()
        {
            Register<IFileProvider,FileProvider>();
            Register<ISearchService,SearchService>();
            Register<ProblemSolver,ProblemSolver>();
        }
        
        private static readonly IDictionary<Type, Type> Types = new Dictionary<Type, Type>();

        private static void Register<TContract, TImplementation>()  { 
            Types[typeof(TContract)] = typeof(TImplementation);
        }

        public static T ResolveService<T>()  {
            return (T)ResolveService(typeof(T));
        }

        private static object ResolveService(Type contract) {
            Type implementation = Types[contract];
            ConstructorInfo constructor = implementation.GetConstructors()[0];
            ParameterInfo[] constructorParameters = constructor.GetParameters();
            if (constructorParameters.Length == 0) { 
                return Activator.CreateInstance(implementation);  
            }

            List<object> parameters = new List<object>(constructorParameters.Length);
            foreach (ParameterInfo parameterInfo in constructorParameters) {  
                parameters.Add(ResolveService(parameterInfo.ParameterType));
            }

            return constructor.Invoke(parameters.ToArray()); 
        }
    }
}