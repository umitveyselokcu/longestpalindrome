﻿using System;
using static SameInReverse.Infrastructure.ServiceCollection;

namespace SameInReverse
{
    internal static class Program
    {
        public static void Main()
        {
            RegisterServices();
            
            ResolveService<ProblemSolver>().Solve();
        }
    }
}

