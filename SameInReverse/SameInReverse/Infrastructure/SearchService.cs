namespace SameInReverse.Infrastructure
{
    public class SearchService : ISearchService
    {
        public string GetLongestPalindrome(string input)
        {
            var resultLength = 0;
            string foundPalindrome = "";
            var inputLength = input.Length;
            for (int palindromeMiddleIndex = 0; palindromeMiddleIndex < inputLength; palindromeMiddleIndex++)
            {
                //checking for odd numbered probabilities
                for (int i = 1; palindromeMiddleIndex - i >= 0 && palindromeMiddleIndex + i < inputLength; i++)
                {
                    if (input[palindromeMiddleIndex - i] != input[palindromeMiddleIndex + i]) break;

                    int currentPalindromeLength = 2 * i + 1;
                    
                    if (currentPalindromeLength <= resultLength) continue;
                    
                    resultLength = currentPalindromeLength;
                    foundPalindrome= input.Substring(palindromeMiddleIndex - i, currentPalindromeLength);
                }
                //checking for even numbered probabilities 
                for (int i = 1;
                    palindromeMiddleIndex - i + 1 >= 0 &&
                    palindromeMiddleIndex + i < inputLength &&
                    palindromeMiddleIndex < inputLength - 1; 
                    i++)
                {
                    if (input[palindromeMiddleIndex - i + 1] != input[palindromeMiddleIndex + i]) break;
                    
                    int currentPalindromeLength = 2 * i;
                    
                    if (currentPalindromeLength <= resultLength) continue;
                    
                    resultLength = currentPalindromeLength;
                    foundPalindrome= input.Substring(palindromeMiddleIndex - i + 1, currentPalindromeLength);
                }
            }

            return foundPalindrome;
        }

    }
}