﻿using System;
using System.IO;

namespace LongestPalingdrome
{
    class Program
    {
        static void Main(string[] args)
        {
        
            string longestPalindrome = "";
            string directory =AppDomain.CurrentDomain.BaseDirectory; //Directory.GetCurrentDirectory();
            string inputPath = Path.Combine(directory, "SameInReverse.sdx");
            string destPath = Path.Combine(inputPath);
            using (StreamReader sr = File.OpenText(destPath))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    longestPalindrome = LongestPalindrome(s);
                 
                }
            }

            FileProvider addddsd = new FileProvider();
            addddsd.WriteToFile(longestPalindrome,"SameInReverseFound.sdx");
         
  Console.WriteLine(longestPalindrome);
            Console.ReadKey();
        }

        private static string LongestPalindrome(string input)
        {
            var palindromeLength = 0;
            var palindrome = "";
            var inputLength = input.Length;
            for (var palindromeMiddleIndex = 0; palindromeMiddleIndex < inputLength; palindromeMiddleIndex++)
            {
                for (var i = 0; palindromeMiddleIndex - i >= 0 && palindromeMiddleIndex + i < inputLength; i++)
                {
                    if (input[palindromeMiddleIndex - i] != input[palindromeMiddleIndex + i]) break;

                    var currentPalindromeLength = 2 * i + 1;
                    
                    if (currentPalindromeLength <= palindromeLength) continue;
                    
                    palindromeLength = currentPalindromeLength;
                    palindrome = input.Substring(palindromeMiddleIndex - i, currentPalindromeLength);
                }
            }

            for (var palindromeMiddleIndex = 0; palindromeMiddleIndex < inputLength - 1; palindromeMiddleIndex++)
            {
                for (var i = 1; palindromeMiddleIndex - i + 1 >= 0 && palindromeMiddleIndex + i < inputLength; i++)
                {
                    if (input[palindromeMiddleIndex - i + 1] != input[palindromeMiddleIndex + i]) break;
                    
                    var currentPalindromeLength = 2 * i;
                    
                    if (currentPalindromeLength <= palindromeLength) continue;
                    
                    palindromeLength = currentPalindromeLength;
                    palindrome = input.Substring(palindromeMiddleIndex - i + 1, currentPalindromeLength);
                }
            }

            return palindrome;
        }

    }


    public class FileProvider
    {
        private string BaseDirectory { get; set; }

        public FileProvider()
        {
            BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        }

        public void WriteToFile(string text,string fileName)
        {
            try
            {

                string path = Path.Combine(BaseDirectory, fileName);
                
                if (File.Exists(path))
                {
                    //writes to file
                    System.IO.File.WriteAllText(path,text);
                }
                else
                {
                    // Create the file.
                    using var fs = File.Create(path);
                    System.IO.File.WriteAllText(path, text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public string ReadFromFile(string fileName)
        {
            try
            {

                string path = Path.Combine(BaseDirectory, fileName);
                
                if (File.Exists(path))
                {
                    //writes to file
                    System.IO.File.WriteAllText(path,"Text to add to the file\n");
                }
                else
                {
                    // Create the file.
                    using (FileStream fs = File.Create(path))
                    {
                        System.IO.File.WriteAllText(path, "Text to add to the file\n");
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return "";
        }
    }
}