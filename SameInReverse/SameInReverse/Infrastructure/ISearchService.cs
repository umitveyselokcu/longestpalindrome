namespace SameInReverse.Infrastructure
{
    public interface ISearchService
    {
        string GetLongestPalindrome(string input);
    }
}