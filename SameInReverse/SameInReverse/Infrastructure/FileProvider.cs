using System;
using System.IO;
using System.Text;

namespace SameInReverse.Infrastructure
{
    public class FileProvider : IFileProvider
    {
        private string BaseDirectory { get; set; }

        public FileProvider()
        {
            BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        }

        public void WriteToFile(string text,string fileName)
        {
            try
            {
                string path = Path.Combine(BaseDirectory, fileName);
                
                if (File.Exists(path))
                {
                    File.WriteAllText(path,text);
                }
                else
                {
                    using (var fs = File.Create(path))
                    {
                    }
                     File.WriteAllText(path, text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public string ReadFromFile(string fileName)
        {
            try
            {
                var path = Path.Combine(BaseDirectory, fileName);
                
                if (File.Exists(path))
                {
                    using (var sr = File.OpenText(path))
                    {
                        string s;
                        var result = "";
                        while ((s = sr.ReadLine()) != null)
                        {
                             result += s;
                        }
                        return result;
                    }
                }
                Console.WriteLine($"ERROR: {fileName} does not exist.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return "";
        }
    }
}